# Popcorn
Popcorn is a linux scheduler that is based on Highest Response Ratio Next (HRRN) policy


# Popcorn Scheduler
Popcorn is based in Highest Response Ratio Next (HRRN) policy.
HRRN is a non-preemptive scheduling policy in which the process
that has the highest response ratio will run next. Each process
has a response ratio value R = (w_t + s_t) / s_t where w_t is
the process waiting time, and s_t is the process estimated running
time. If two process has similar estimated running times, the
process that has been waiting longer will run first. HRRN aims
to prevent starvation since it strives the waiting time for processes.
    
Popcorn calculates the heat for each process which is h = R + B
 		where R is the response ratio and B is a boost value that is given
 		to newly created processes. The scheduler will pick next process
 		to run which has the highest value of h. See (calcHeat) function
 		in kernel/sched/fair.c

CFS red black tree is used to sort out processes by their heat
 		values except that the comparison is reversed (i.e. leftmost
 		has higher value). See (entity_before) function in kernel/sched/fair.c

CFS way for calculating processes' vruntime, weights, and timeslices
		are used in where CFS already takes care of assigning a process a
		vruntime with respect of its priority weight. High priority processes
		will have less vruntime which is also has effects on calculating
		the response ratio where w_t = (current_time - creation_time) - vruntime,
		and s_t is basically the process vruntime. High priority processes
		will have higher response ratio.

boost: is a value given to a newly created task/process in such
 		that created process will have more chance to run for limited
 		time. The value 65535 is given and will be added to calculate
 		heat (calcHeat). Every time process is picked to run, boost is
 		reduced by one right bit-shift (i.e. 65535 >> 1). See (__sched_fork)
 		function in kernel/sched/core.c

boost values:
* 65535
* 32767
* 16383
* 8191
* 4095
* 2047
* 1023
* 511
* 255
* 127
* 63
* 31
* 15
* 7
* 3
* 1
* 0


# How to apply the patch
* Download the linux kernel (https://www.kernel.org/) that is same version as the patch (i.e if patch file name is popcorn-4.14.10.patch, then download https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.14.10.tar.xz)
* Unzip linux kernel
* Download popcorn patch file and place it inside the just unzipped linux kernel folder
* cd linux-(version)
* patch -p1 < popcorn-4.14.10.patch

To build the kernel you need to follow linux build kernel tutorials 
